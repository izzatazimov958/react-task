import React, { useState, useEffect } from "react"; // eslint-disable-line
import "./navbar.scss";
import axios from "axios";

const Navbar = () => {
  const [categories, setCategories] = useState(null); // eslint-disable-line

  const getCategories = () => {
    // eslint-disable-line
    axios
      .get(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/categories`)
      .then((res) => {
        // console.log("response is ", res);
        setCategories(res.data);
      });
  };

  useEffect(() => {
    getCategories();
  }, []);

  return (
    <nav className="nav">
      <div className="container">
        <ul className="nav__menu">
          {categories &&
            categories.map((category) => {
              return (
                <li key={category.id} className="nav__menu-item">
                  <a href={category.name}>{category.name}</a>
                </li>
              );
            })}
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
