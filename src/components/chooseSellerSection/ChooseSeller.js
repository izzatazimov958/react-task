import "./chooseseller.scss";
import axios from "axios";
import React, { useState, useEffect } from "react"; // eslint-disable-line

const ChooseSeller = () => {
  const [allProduct, setAllProduct] = useState(false); // eslint-disable-line
  const [products, setProducts] = useState([]); // eslint-disable-line

  const getProducts = () => {
    // eslint-disable-line
    axios
      .get(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products`)
      .then((res) => {
        setProducts(res.data);
      });
  };

  useEffect(() => {
    getProducts();
  }, []);

  const limitedProducts = products.slice(0, 6).map(
    (
      product // eslint-disable-line
    ) => (
      <div key={product.id} className="product">
        <div className="product__image">
          <img src={product.image} alt={product.name} />
        </div>
        <div className="product__info">
          <h3 className="product__name">{product.name}</h3>
          <p className="product__old-price">{product.old_price} old price</p>
          <p className="product__price">Price: {product.price} </p>
          <p className="product_monthly-pay">
            {product.monthly_pay} monthly pay
          </p>
          <button className={`btn ${product.in_stock === 0 && `active`}  `}>
            В корзину
          </button>
        </div>
      </div>
    )
  );

  const allProducts = products.map((product) => (
    <div key={product.id} className="product">
      <div className="product__image">
        <img src={product.image} alt={product.name} />
      </div>
      <div className="product__info">
        <h3 className="product__name">{product.name}</h3>
        <p className="product__old-price">{product.old_price} old price</p>
        <p className="product__price">Price: {product.price} </p>
        <p className="product_monthly-pay">{product.monthly_pay} monthly pay</p>
        <p className="product_inStock">{product.in_stock} in stock</p>
        <button className={`btn ${product.in_stock === 0 ? `active` : ""}  `}>
          В корзину
        </button>
      </div>
    </div>
  ));

  return (
    <section className="chooseSeller">
      <div className="container">
        <div className="title-n-button">
          <h2 className="chooseSeller__title">Новинки</h2>
          <p className="setter" onClick={() => setAllProduct(!allProduct)}>
            {allProduct ? "Смотреть некоторые" : "Смотреть все"}
          </p>
        </div>
        <div className="chooseSeller__products">
          {allProduct ? allProducts : limitedProducts}
        </div>
      </div>
    </section>
  );
};

export default ChooseSeller;
