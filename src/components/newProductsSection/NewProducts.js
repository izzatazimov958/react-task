import React from "react";
import Product from "../Products/Product";
import "./newproducts.scss";

const NewProducts = () => {
  return <section className="newproducts">
    <div className="container">
      <h2 className="newproducts__title">Новинки</h2>
      <div className="newproducts__products">
        <Product />
      </div>
    </div>
  </section>;
};

export default NewProducts;
