import React, { useState, useEffect } from "react"; // eslint-disable-line  

import "./product.scss";
import axios from "axios";

const Product = () => {
  const [products, setProducts] = useState(null); // eslint-disable-line
  const getProducts = () => {
    // eslint-disable-line
    axios
      .get(`https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products`)
      .then((res) => {
        setProducts(res.data);
      });
  };

  useEffect(() => {
    getProducts();
  }, []);

  // console.log("products are ", products);

  return (
    <section className="products-section">
      {products &&
        products
          .filter((product) => product.status === "new")
          .map((product) => {
            return (
              <div key={product.id} className="product">
                <div className="product__image">
                  <img src={product.image} alt={product.name} />
                </div>
                <div className="product__info">
                  <h3 className="product__name">{product.name}</h3>
                  <p className="product__old-price">
                    {product.old_price} old price
                  </p>
                  <p className="product__price">Price: {product.price} </p>
                  {/* {product.monthly_pay && ( */}
                  <p className="product_monthly-pay">
                    {product.monthly_pay} monthly pay
                  </p>
                  {/* // )} */}
                  <button
                    className={`btn ${product.in_stock === 0 && `active`}  `}
                  >
                    В корзину
                  </button>
                </div>
              </div>
            );
          })}
    </section>
  );
};

export default Product;
// axios.get(‘https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products)
