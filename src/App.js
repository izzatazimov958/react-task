import './App.scss';
import ChooseSeller from './components/chooseSellerSection/ChooseSeller';
import Navbar from './components/navbar/Navbar';
import NewProducts from './components/newProductsSection/NewProducts';


function App() {
  return (
    <div className="App">
      <Navbar />      
      <NewProducts />
      <ChooseSeller />
    </div>
  );
}

export default App;
